if (!global.hasOwnProperty('redis')) {
    const redis = require("redis");
    const redis_port = process.env.REDIS_PORT || 4000;
    const redis_client = redis.createClient(redis_port);
    
    console.log('Redis server started on port:' + redis_port)
    global.redis = global.redis ? global.redis : redis_client;
  }
  
  module.exports = global.redis
  
const Canvas = require('canvas');
const Discord = require('discord.js');
const embed = new Discord.MessageEmbed()

module.exports = {
  name: 'gslot',
  description: 'this command gives ping',
  async execute(message, args) {

    if (!message.member.hasPermission("ADMINISTRATOR")) {
      message.channel.send('You need admin rights to execute this command')
      return;
    }

    message.channel.send(`Processing image, please wait`);
    let teams = message.cleanContent.split('\n');
    var background = await Canvas.loadImage(`./paid_tournament/slotlist_template/slot_list.png`);
    var canvas = Canvas.createCanvas(background.width, background.height);
    var ctx = canvas.getContext('2d');
    ctx.drawImage(background, 0, 0, background.width, background.height);

    for (let i = 0; i < (teams.length-1)/2; i++) {

      ctx.fillStyle = '#FFFFFF';
      ctx.font = '28px REDWING MEDIUM';
      ctx.textAlign = "left";
      ctx.fillText(teams[i+1].toUpperCase(), 184, 275 + (65.5 * i));
      ctx.fillText(teams[i+9].toUpperCase(), 532, 275 + (65.5 * i))
    }

    var attachment = new Discord.MessageAttachment(canvas.toBuffer(), `slot_list.png`);
    message.channel.send(`Slot List by MRK ESPORTS BOT.`, attachment)


  }
}
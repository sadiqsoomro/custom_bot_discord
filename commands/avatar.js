const Discord = require('discord.js');
const embed = new Discord.MessageEmbed()

module.exports = {
    name: 'avatar',
    description: 'this command gives ping',
    execute(message, args) {
        let member = ""
        if (message.mentions.users.size == 0)
        member = message.member
        else
        member = message.mentions.members.first() 
        let username = member.user.tag
        let displayName =member.displayName;
        let userID = member.user.id
        let joinedDateTime = member.joinedAt.toLocaleTimeString("en-us", options = {
            weekday: "long", year: "numeric", month: "short",
            day: "numeric"
        })
        embed.setDescription(`**${username}**
\`\`\`Nickname: ${displayName}
ID: ${userID}
Joined on: ${joinedDateTime}\`\`\``)
            .setImage(member.user.displayAvatarURL({ size: 2048,dynamic : true }))

        message.channel.send(embed)
    }
}
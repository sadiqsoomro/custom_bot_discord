const Canvas = require('canvas');
const Discord = require('discord.js');
const embed = new Discord.MessageEmbed()
const axios = require('axios');
const baseURL = process.env.URL;

function imageConfig(configName) {
  if (configName == '24')
    return {
      height: 232,
      difference: 42.8,
      baseWidth: 178,
      matchesPlayedWidth: 350,
      placePointsWidth: 485,
      killPointsWidth: 610,
      totalPointsWidth: 735,
      headerWidth: 370,
      headerHeight: 110,
      footerWidth: 65,
      footerHeight: 1255
    }
  else if (configName == '18')
    return {
      height: 252,
      difference: 47.2,
      baseWidth: 200,
      matchesPlayedWidth: 380,
      placePointsWidth: 525,
      killPointsWidth: 670,
      totalPointsWidth: 805,
      headerWidth: 350,
      headerHeight: 130,
      footerWidth: 75,
      footerHeight: 1105
    }
  else if (configName == '17')
    return {
      height: 252,
      difference: 47,
      baseWidth: 200,
      matchesPlayedWidth: 380,
      placePointsWidth: 525,
      killPointsWidth: 670,
      totalPointsWidth: 805,
      headerWidth: 350,
      headerHeight: 130,
      footerWidth: 75,
      footerHeight: 1050
    }
}
module.exports = {
  name: 'gresult',
  description: 'this command gives ping',
  async execute(message, args) {

    if (!message.member.hasPermission("ADMINISTRATOR")) {
      message.channel.send('You need admin rights to execute this command')
      return;
    }
    //message.delete({ timeout: 3000 })
    let imageCaption = '';
    let imageTitle = '';
    let competition = "";
    let week = process.env.SEASON;
    let tier = "";
    let round = "";
    let group = "";
    let match = "";
    let imageName = '';
    let day = ''
    let filter = m => m.author.id === message.author.id
    const logo = message.guild.iconURL();
    embed.setAuthor(`RGE by MRK ESPORTS BOT`, logo, 'https://mrkesports.com')
      .setDescription(`Please select competition: (Reply With Number)
1- OBS
2- PMAS`)
      .setColor(0x3B5998)
      .setThumbnail(logo)
      .setFooter("You have 30 seconds to reply.");

    try {
      let initialPrompt = await message.channel.send(embed);
      let initialResponse = await message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
      initialResponse = initialResponse.first()
      if (!(initialResponse.cleanContent == '1' || initialResponse.cleanContent == '2')) {
        message.channel.send(`Invalid response, terminating the request.`)
        return;
      }
      if (initialResponse.cleanContent == '1') competition = `obs`
      else if (initialResponse.cleanContent == '2') competition = `pmas`


      if (competition == 'pmas') {
        imageName = 'pmas'
        initialPrompt.delete({ timeout: 3000 })
        initialResponse.delete({ timeout: 3000 })
        dayPrompt = `
0- Final Standings
1- Day 1
2- Day 2
3- Day 3`;
        embed.setDescription(`Please select day: (Reply With Number) ${dayPrompt}`)
        let dayInput = await message.channel.send(embed);
        let dayResponse = await message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
        dayResponse = dayResponse.first()
        if (!(dayResponse.cleanContent == '0' || dayResponse.cleanContent == '1' || dayResponse.cleanContent == '2' || dayResponse.cleanContent == '3')) {
          message.channel.send(`Invalid response, terminating the request.`)
          return;
        }
        day = dayResponse.cleanContent
        if (day == 0) {
          day = 'weekly_standing';
          imageTitle = "Week 5";
        }
        else {
          imageTitle = `Day ${day} `
          dayInput.delete({ timeout: 3000 })
          dayResponse.delete({ timeout: 3000 })
          matchPrompt = `
0- Final Standings
1- Match 1
2- Match 2
3- Match 3`;

          embed.setDescription(`Please select match: (Reply With Number) ${matchPrompt}`)
          let matchInput = await message.channel.send(embed);
          let matchResponse = await message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
          matchResponse = matchResponse.first()
          if (!(matchResponse.cleanContent == '0' || matchResponse.cleanContent == '1' || matchResponse.cleanContent == '2' || matchResponse.cleanContent == '3')) {
            message.channel.send(`Invalid response, terminating the request.`)
            return;
          }

          match = matchResponse.cleanContent;
          if (match == 0) {
            match = 'daily_standing'
            imageTitle = imageTitle + ``
          }
          else imageTitle = imageTitle + `Match ${match}`
          matchInput.delete({ timeout: 3000 })
          matchResponse.delete({ timeout: 3000 })
        }
      } else if (competition == 'obs') {
        imageName = 'obs'
        initialPrompt.delete({ timeout: 3000 })
        initialResponse.delete({ timeout: 3000 })
        roundPrompt = `
1- Qualifiers
2- Redzone-1
3- Quarter Finals
4- Redzone-2
5- Elites
6- Int. Elites
7- FINALS
8- Grand Finals`;
        embed.setDescription(`Please select round: (Reply With Number) ${roundPrompt}`)
        let roundInput = await message.channel.send(embed);
        let roundResponse = await message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
        roundResponse = roundResponse.first()
        if (!(roundResponse.cleanContent == '1' || roundResponse.cleanContent == '2' || roundResponse.cleanContent == '3' || roundResponse.cleanContent == '4' || roundResponse.cleanContent == '5' || roundResponse.cleanContent == '6' || roundResponse.cleanContent == '7'|| roundResponse.cleanContent == '8')) {
          message.channel.send(`Invalid response, terminating the request.`)
          return;
        }
        round = roundResponse.cleanContent
        if (round == 1) {
          round = 'qualifiers';
          imageTitle = "qualifiers";
        }
        else if (round == 2) {
          round = 'redzone1';
          imageTitle = "redzone1";
        } else if (round == 3) {
          round = 'quarter';
          imageTitle = "quarter";
        } else if (round == 4) {
          round = 'redzone2';
          imageTitle = "redzone2";
        } else if (round == 5) {
          round = 'elite';
          imageTitle = "elite";
        }else if (round == 6) {
          round = 'international';
          imageTitle = "international";
        }else if (round == 7) {
          round = 'pfinals';
          imageTitle = "pfinals";
        }else if (round == 8) {
          round = 'finals';
          imageTitle = "finals";
        }
        roundInput.delete({ timeout: 3000 })
        roundResponse.delete({ timeout: 3000 })


        groupPrompt = `
1- Group A
2- Group B
3- Group C
4- Group D
5- Group E
6- Group F
7- Group G
8- Group H
9- Group I
10- Group J`;
        embed.setDescription(`Please select group: (Reply With Number) ${groupPrompt}`)
        let groupInput = await message.channel.send(embed);
        let groupResponse = await message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
        groupResponse = groupResponse.first()
        if (!(groupResponse.cleanContent == '1' || groupResponse.cleanContent == '2' || groupResponse.cleanContent == '3' || groupResponse.cleanContent == '4' || groupResponse.cleanContent == '5' || groupResponse.cleanContent == '6' || groupResponse.cleanContent == '7' || groupResponse.cleanContent == '8' || groupResponse.cleanContent == '9' || groupResponse.cleanContent == '10')) {
          message.channel.send(`Invalid response, terminating the request.`)
          return;
        }
        group = groupResponse.cleanContent
        if (group == 1) group = "A"
        else if (group == 2) group = "B"
        else if (group == 3) group = "C"
        else if (group == 4) group = "D"
        else if (group == 5) group = "E"
        else if (group == 6) group = "F"
        else if (group == 7) group = "G"
        else if (group == 8) group = "H"
        else if (group == 9) group = "I"
        else if (group == 10) group = "J"
        imageCaption = `Group ${group}`;
        groupInput.delete({ timeout: 3000 })
        groupResponse.delete({ timeout: 3000 })

        matchPrompt = `
1- Match 1
2- Match 2
3- Match 3
4- Match 4
5- Match 5
6- Match 6
7- Match 7
8- Match 8
9- Match 9
10- Match 10
11- Match 11
12- Match 12`;

        embed.setDescription(`Please select match: (Reply With Number) ${matchPrompt}`)
        let matchInput = await message.channel.send(embed);
        let matchResponse = await message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
        matchResponse = matchResponse.first()
        if (!(matchResponse.cleanContent == '1' || matchResponse.cleanContent == '2' || matchResponse.cleanContent == '3' || matchResponse.cleanContent == '4' || matchResponse.cleanContent == '5' || matchResponse.cleanContent == '6' || matchResponse.cleanContent == '7' || matchResponse.cleanContent == '8' || matchResponse.cleanContent == '9' || matchResponse.cleanContent == '10' || matchResponse.cleanContent == '11' || matchResponse.cleanContent == '12')) {
          message.channel.send(`Invalid response, terminating the request.`)
          return;
        }

        match = matchResponse.cleanContent;
        imageTitle = imageTitle + `Match ${match}`
        imageCaption = imageCaption + "- Match "+match;
        matchInput.delete({ timeout: 3000 })
        matchResponse.delete({ timeout: 3000 })
      }
      let url = "";
      let options;
      if (competition == 'obs') {
        url = '/v1/getPointsTableOBS'
        options = {
          round_name: round,
          group_name: group,
          match: match
        }
      } else if (competition == 'pmas') {
        url = '/v1/getPointsTablePMAS'
        options = {
          week: '5',
          matches: match,
          days: day
        }
      }

      let fetchingResultsPrompt = await message.channel.send(`Fetching results, please wait.`);
      let response = await axios.post(baseURL + url, options)

      var recievedResponse = response.data.current;
      if (recievedResponse.length == 0) {
        message.channel.send(`No data found!`)
        return;
      }
      fetchingResultsPrompt.edit(`Processing image, please wait`);
      if (competition == 'obs') {

        let data = recievedResponse;
        //let prevData = response.data.previous;
        let imageData = {};
        for (let i = 0; i < data.length; i++) {
          var team_name = data[i]['team_name'].toString().toUpperCase().trim();
          //console.log(team_name);
          imageData[team_name] = await Canvas.loadImage(`./paid_tournament/team_logos/${team_name}.png`)

        }
        //let position1 = "", position2 = "";
        let firstLoop;
        let secondLoop;
        let image = await Canvas.loadImage(`./paid_tournament/team_logos/test.png`)
        if(data.length == 17){
          firstLoop = 9;
          secondLoop = 8;
        }else{
          firstLoop = 10;
          secondLoop = 10;
        }
        var background1 = await Canvas.loadImage(`./paid_tournament/result_template/obs_${round}${firstLoop}.png`);
        var canvas1 = Canvas.createCanvas(background1.width, background1.height);
        var ctx1 = canvas1.getContext('2d');
        ctx1.drawImage(background1, 0, 0, background1.width, background1.height);
        
        //#023181
        ctx1.fillStyle = '#023181';
        ctx1.font = '70px AGENCYFB';
        ctx1.textAlign = "right";
        ctx1.fillText("GRAND", 1477, 189);
       
        ctx1.textAlign = "left";
        ctx1.fillStyle = '#000000';
        ctx1.font = '80px AGENCYFB';
        ctx1.fillText("AFTER GAME "+match, 830, 180);
       
        for (let i = 0; i < firstLoop; i++) {

          ctx1.fillStyle = '#000000';
          ctx1.font = '50px AGENCYFB';
          ctx1.textAlign = "right";

          var difference = 73;
          var lYaxis = 330;
          ctx1.drawImage(imageData[data[i]["team_name"].toString().toUpperCase().trim()], 310, lYaxis-50 + (difference * i), 60, 60);
          //ctx.drawImage(, 265, lYaxis-40 + (difference * i), 55, 55);
          ctx1.fillText((i+1).toString(), 235, lYaxis + (difference * i));
          ctx1.textAlign = "left";
          ctx1.fillText(data[i]["team_name"].toUpperCase(), 435, lYaxis + (difference * i));
          ctx1.textAlign = "left";
          ctx1.fillText(data[i]["match_played"] , 845, lYaxis + (difference * i));
          ctx1.fillText(data[i]["total_position_points"] , 1085, lYaxis + (difference * i));
          ctx1.fillText(data[i]["total_kill_points"] , 1290, lYaxis + (difference * i));
          ctx1.fillText(data[i]["total_points"], 1495, lYaxis + (difference * i));
          ctx1.fillText(data[i]['total_wwcd'], 1690, lYaxis + (difference * i));
          
        }
        ctx1.fillText("GENERATED BY MRK ESPORTS BOT.", 155, 1065);

        var attachment = new Discord.MessageAttachment(canvas1.toBuffer(), `${imageCaption}.png`);
        message.channel.send(imageCaption, attachment)

        var background2 = await Canvas.loadImage(`./paid_tournament/result_template/obs_${round}${secondLoop}.png`);
        var canvas2 = Canvas.createCanvas(background2.width, background2.height);
        var ctx2 = canvas2.getContext('2d');
        ctx2.drawImage(background2, 0, 0, background2.width, background2.height);
        ctx2.fillStyle = '#023181';
        ctx2.font = '70px AGENCYFB';
        ctx2.textAlign = "right";
        ctx2.fillText("GRAND", 1477, 189);
       
        ctx2.textAlign = "left";
        
        ctx2.fillStyle = '#000000';
        ctx2.font = '80px AGENCYFB';
        ctx2.fillText("AFTER GAME "+match, 830, 180);


        for (let i = 0; i < secondLoop; i++) {

          ctx2.fillStyle = '#000000';
          ctx2.font = '50px AGENCYFB';
          ctx2.textAlign = "right";

          var difference = 73;
          var lYaxis = 330;
          ctx2.drawImage(imageData[data[i+firstLoop]["team_name"].toString().toUpperCase().trim()], 310, lYaxis-50 + (difference * i), 60, 60);
          ctx2.fillText((i+1+firstLoop).toString(), 235, lYaxis + (difference * i));
          ctx2.textAlign = "left";
          
          ctx2.fillText(data[i+firstLoop]["team_name"].toUpperCase(), 435, lYaxis + (difference * i));
          ctx2.textAlign = "left";
          ctx2.fillText(data[i+firstLoop]["match_played"] , 845, lYaxis + (difference * i));
          ctx2.fillText(data[i+firstLoop]["total_position_points"] , 1085, lYaxis + (difference * i));
          ctx2.fillText(data[i+firstLoop]["total_kill_points"] , 1290, lYaxis + (difference * i));
          ctx2.fillText(data[i+firstLoop]["total_points"], 1495, lYaxis + (difference * i));
          ctx2.fillText(data[i+firstLoop]['total_wwcd'], 1690, lYaxis + (difference * i));
          
        }
        ctx2.fillText("GENERATED BY MRK ESPORTS BOT.", 155, 1065);
        var attachment2 = new Discord.MessageAttachment(canvas2.toBuffer(), `${imageCaption}.png`);
        message.channel.send(imageCaption, attachment2)



          //ctx.textAlign = "left";
          //ctx.fillText("GROUP " + group, 82, 250);
          //ctx.textAlign = "right";
          //ctx.fillText("MATCH " + match, 1839, 250);
          //ctx.fillText("MATCH " + match, 780, 349);


      
      

      } else {
        let imageData = {};
        for (let i = 0; i < response.data.response.length; i++) {
          var team_name = response.data.response[i]['team_name'];
          imageData[team_name] = await Canvas.loadImage(`./pmas/team_logos/${team_name}.png`)

        }
        var wwcdImage = await Canvas.loadImage(`./pmas/chicken_dinner/wwcd.png`);

        try {
          for (let mainLoop = 0; mainLoop <= 1; mainLoop++) {
            var background = await Canvas.loadImage(`./pmas/result_template/result_template.png`);
            var canvas = Canvas.createCanvas(background.width, background.height);
            var ctx = canvas.getContext('2d');
            ctx.drawImage(background, 0, 0, background.width, background.height);
            ctx.fillStyle = '#FFFFFF';
            ctx.font = 'bold 75px REDWING MEDIUM';
            ctx.fillText("Overall Standings", 170, 400);
            ctx.textAlign = "right";
            ctx.fillText(`${imageTitle}`, 1850, 400);
            ctx.textAlign = "left";
            let data = response.data.response;
            for (let i = 0; i < 10; i++) {
              ctx.font = '30px REDWING MEDIUM';

              ctx.fillStyle = '#000000';
              ctx.fillText("#" + ((i + 1) + (10 * mainLoop)), 222, 560 + (81 * i));
              ctx.fillStyle = '#FFFFFF';
              ctx.fillText(data[i + (10 * mainLoop)]['team_name'], 222 + 140, 560 + (81 * i));

              ctx.fillStyle = '#000000';
              ctx.fillText(data[i + (10 * mainLoop)]['match_played'], 222 + 740, 560 + (81.1 * i));
              ctx.fillText(data[i + (10 * mainLoop)]['position_points'], 222 + 955, 560 + (81.1 * i));
              ctx.fillText(data[i + (10 * mainLoop)]['kill_points'], 222 + 1178, 560 + (81.1 * i));
              ctx.fillText(data[i + (10 * mainLoop)]['total_points'], 222 + 1445, 560 + (81.1 * i));
              ctx.drawImage(imageData[data[i + (10 * mainLoop)]['team_name']], 700, 522 + (81.2 * i), 62, 62);
              if (data[i + (10 * mainLoop)]['wwcd'] > 0) {
                ctx.drawImage(wwcdImage, 222 + 1525, 515 + (81.1 * i), 62, 62);
                ctx.font = '20px REDWING MEDIUM';
                ctx.fillText("x" + data[i + (10 * mainLoop)]['wwcd'], 222 + 1590, 565 + (81.1 * i));
              }
            }
            ctx.font = '30px REDWING MEDIUM';
            ctx.fillStyle = '#FFFFFF';
            ctx.fillText("-GENERATED BY MRK ESPORTS BOT.", 20, 1490);
            var attachment = new Discord.MessageAttachment(canvas.toBuffer(), `${imageTitle}_${mainLoop + 1}.png`);
            message.channel.send(`${imageTitle}-${mainLoop + 1}`, attachment)
          }
        } catch (error) {
          console.log(error);
        }
      }

    }
    catch (error) {
      console.log(error);
      message.channel.send('Request timed out, you didn\'t respond within 30 seconds.');
    }
  }
}
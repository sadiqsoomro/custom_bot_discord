const embed = require('../utils/embed')
const discord = require('discord.js');
const message = require('../events/message');

module.exports = {
    name: 'send',
    description: 'this command gives ping',
    execute(oldMessage,newMessage, args, isUpdate) {

        var channel = newMessage.mentions.channels.first();
        
        if(!channel){
            newMessage.reply(`Did you forget to tag channel bro?`)
            return;
        }
        var roles = newMessage.mentions.roles.map(role=> role);
        var users = newMessage.mentions.users.map(user=> user);
        let mentions = "";
        for(let i = 0 ; i < roles.length; i++)
            mentions = `<@&${roles[i].id}> `+mentions
        
        for(let i = 0 ; i < users.length; i++)
            mentions = `<@${users[i].id}> `+mentions


        const embed = new discord.MessageEmbed();
        embed.setColor(0x3B5998);

        let content = newMessage.content;
        let oldMessageContent = oldMessage.content.substring(oldMessage.content.indexOf('\n') + 1);
        let emojis = Array();
        let splitted = content.split('\n');


        for (let i = 1; i < splitted.length; i++)
            if (splitted[i].includes("<:") || splitted[i].includes("<a:"))
                emojis.push(splitted[i].split('>')[0].trim());
        embed.setDescription(content.substring(content.indexOf('\n') + 1));
        let messageHistory = channel.messages.cache.map(mesg => mesg);
        if (messageHistory[0] && isUpdate)
        {
            for(let x = 0 ; x < messageHistory.length;x++ ){
                if(messageHistory[x].embeds)
                if(messageHistory[x].embeds[0].description == oldMessageContent){
                    messageHistory[x].edit(embed)
                    break;
                }
            }
        }
        if(newMessage.attachments.size >0){
            embed.setImage(newMessage.attachments.first().url)
        }else
        embed.setThumbnail(newMessage.guild.iconURL({dynamic : true}))


        if (!isUpdate) {
            channel.send(mentions,embed).then(sentEmbed => {
                for (let x = 0; x < emojis.length; x++)
                    sentEmbed.react(emojis[x]);
            });
        }

        
    }
}
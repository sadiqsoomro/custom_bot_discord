module.exports = function (client,prefix) {
    client.on("messageReactionAdd", (reaction, user) => { // When a reaction is added
        if (user.bot) 
        return; // If the user who reacted is a bot, return
        
        if(reaction)
        if (reaction.message.channel.id == "811614521671548969") {
            reaction.message.guild.members.fetch(user.id).then(fetched=>{
                var Role = reaction.message.guild.roles.cache.find(role => role.name.toLowerCase() == reaction.emoji.name.toLowerCase());
                if (Role)
                fetched.roles.add(Role)
                .catch(err=> {console.log(err) })
            });
        }
    });
}
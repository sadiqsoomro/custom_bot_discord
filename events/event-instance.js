

module.exports = function (client,prefix) {
    require('./message')(client,prefix)
    require('./messageUpdate')(client,prefix)
    require('./messageReactionAdd')(client,prefix)
    require('./messageReactionRemove')(client,prefix)
    require('./raw')(client,prefix)
    require('./ready')(client,prefix)
    if(process.env.IS_PRODUCTION == 'true'){
    require('./guildMemberRemove')(client,prefix)
    require('./guildMemberAdd')(client,prefix)
    }
    require('./debug')(client,prefix)
    
}
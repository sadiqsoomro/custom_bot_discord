const Canvas = require("discord-canvas");
const Discord = require('discord.js');
const embed = new Discord.MessageEmbed();
const utils = require('../utils/utils')
module.exports = function (client,prefix) {
    client.on('guildMemberAdd', async member => {
        if(process.env.IS_PRODUCTION == 'true')
        {
        const channel =  utils.FindChannelByIDFromMember(member,'811294762044227587')
        const voiceChannel =  utils.FindChannelByIDFromMember(member,'811294761595699267')
        
        if(voiceChannel)
        voiceChannel.setName(`Member Count: ${member.guild.memberCount}`)
        
        if (!channel)
            return;
            const image = await new Canvas.Welcome()
            .setUsername(member.user.username)
            .setDiscriminator(member.user.discriminator)
            .setMemberCount(member.guild.memberCount)
            .setGuildName("OPS Gaming")
                    .setAvatar(member.user.displayAvatarURL({
                        format: 'jpg'
                    }))
                    .setColor("border", "#000000")
                    .setColor("username-box", "#000000")
                    .setColor("discriminator-box", "#000000")
                    .setColor("message-box", "#000000")
                    .setColor("title", "#ffffff")
                    .setColor("avatar", "#000000")
                    .setBackground("https://mrkesports.com/public_images/welcome-image.png")
                    .toAttachment();

                const attachment = new Discord.MessageAttachment(image.toBuffer(), "welcome-image.png");

                embed
                    .setThumbnail(member.guild.iconURL())
                    .setTitle(`**OPS Gaming**`)
                    .setColor(0x3B5998)
                    .setDescription(`Welcome to OPS Gaming, may you have a good time here.
Before moving forward, please checkout our:
[INSTAGRAM](https://instagram.com/opsgaming.official)
[YOUTUBE](https://youtube.com/c/OPSGaming786)`)
                    .setFooter(`Together We Grow!`);
                    channel.send(`Welcome, <@${member.user.id}> to OPS Gaming!`, attachment);



        member.user.send(embed).catch(error => {
            console.log("Error in DM: " + error);
        });
    }
    });
}
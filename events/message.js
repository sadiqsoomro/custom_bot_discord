const utils = require("../utils/utils")



module.exports = function (client, prefix, spreadSheetClient, google) {
    
    client.on("message", async message => {

        const args = message.content.slice(prefix.length).split(/ +/);
        let command = utils.CommandValidator(args.shift().toLowerCase());
        
        if (!message.content.startsWith(prefix) || message.author.bot) {
          return;
        }

       if (command === "gresult") {
            client.commands.get('gresult').execute(message, args);
        }
        else if (command === 'join') {
            client.emit('guildMemberAdd', message.member);
        }
        else if (command === "send") {
            client.commands.get('send').execute(message, message, args, false);
        } 
        else if (command === "addemoji") {
            client.commands.get('addemoji').execute(message,  args);
        } 
        else if (command === "avatar") {
            client.commands.get('avatar').execute(message, args);
        }  else if (command === 'join') {
            client.emit('guildMemberAdd', message.member);
        }else if (command === "gtest") {
            client.commands.get('gtest').execute(message, args);
        }else if (command === "gslot") {
            client.commands.get('gslot').execute(message, args);
        }
        
    });
}
const utils = require("../utils/utils")

module.exports = function (client,prefix) {
    client.on('messageUpdate', (oldMessage, newMessage) => {
        const args = newMessage.content.slice(prefix.length).split(/ +/);
        let command = utils.CommandValidator(args.shift().toLowerCase());
    
        if (!newMessage.content.startsWith(prefix) || newMessage.author.bot) {
            return;
        }
    
         if (command === "send" ) {
            client.commands.get('send').execute(oldMessage,newMessage, args,true);
        }
    });
}
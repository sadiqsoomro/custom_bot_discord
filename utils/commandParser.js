let helper = require('./utils');


function ResultsParser(command) {
    let data = {};
    data["error"] = "";
    var splittedData = command.split("_"); {
        if (splittedData[1]) {
            if (splittedData[1].trim().charAt(0).toLowerCase() != "w") {
                data["error"] = "Error: 1st argument should be week eg. week5, argument provided: " + splittedData[1];
                return data;
            } else if (!Number.isInteger(parseInt(splittedData[1].substr(splittedData[1].length - 1)))) {
                data["error"] = "Error: 1st argument is missing week number eg. week5, argument provided: " + splittedData[1];
                return data;
            }
        } else {
            data["error"] = "Error: 1st argument week is missing";
            return data;
        }

        if (splittedData[2]) {
            if (splittedData[2].trim().charAt(0).toLowerCase() != "t") {
                data["error"] = "Error: 2nd argument should be tier eg. tier2, argument provided: " + splittedData[1];

                return data;
            } else if (!Number.isInteger(parseInt(splittedData[2].substr(splittedData[2].length - 1)))) {
                data["error"] = "Error: 2nd argument is missing tier number eg. tier3, argument provided: " + splittedData[2];
                return data;
            }
        } else {
            data["error"] = "Error: 2nd argument tier is missing";
            return data;
        }


        if (splittedData[3]) {
            if (splittedData[3].trim().charAt(0).toLowerCase() != "g") {
                data["error"] = "Error: 3rd argument should be group eg. groupA, argument provided: " + splittedData[3];
                return data;
            }

        } else {
            data["error"] = "Error: 3rd argument group is missing";
            return data;
        }


        if (splittedData[4]) {
            if (splittedData[4].trim().toLowerCase() == "fs") {} else if (splittedData[4].trim().charAt(0).toLowerCase() != "m") {
                data["error"] = "Error: 4th argument should be match eg. match1, argument provided: " + splittedData[4];
                return data;
            } else if (!Number.isInteger(parseInt(splittedData[4].substr(splittedData[4].length - 1)))) {
                data["error"] = "Error: 4th argument is missing match number eg. match1, argument provided: " + splittedData[4];
                return data;
            }
        } else {
            data["error"] = "Error: 4th argument match is missing";
            return data;
        }


        if (splittedData[5]) {
            if (splittedData[5].trim() == "") {
                data["error"] = "Error: 5th argument should be team name eg. MRK, argument provided: " + splittedData[5];
                return data;
            }
        } else {
            data["error"] = "Error: 5th argument team name is missing";
            return data;
        }

        data["season"] = splittedData[1].substr(splittedData[1].length - 1);
        data["tier"] = splittedData[2].substr(splittedData[2].length - 1);
        data["group"] = splittedData[3].substr(splittedData[3].length - 1);

        if (splittedData[4].trim().toLowerCase() != "fs")
            data["match"] = splittedData[4].substr(splittedData[4].length - 1);
        else
            data["match"] = splittedData[4].trim().toLowerCase();

        data["teamName"] = splittedData[5].trim();

        return data;
    }
}



function ResParser(command) {
    let data = {};
    data["error"] = "";
    var splittedData = command.split("_"); {
        if (splittedData[1]) {
            if (splittedData[1].trim().charAt(0).toLowerCase() != "w") {
                data["error"] = "Error: 1st argument should be week eg. week5, argument provided: " + splittedData[1];
                return data;
            } else if (!Number.isInteger(parseInt(splittedData[1].substr(splittedData[1].length - 1)))) {
                data["error"] = "Error: 1st argument is missing week number eg. week5, argument provided: " + splittedData[1];
                return data;
            }
        } else {
            data["error"] = "Error: 1st argument week is missing";
            return data;
        }

        if (splittedData[2]) {
            if (splittedData[2].trim().charAt(0).toLowerCase() != "d") {
                data["error"] = "Error: 2nd argument should be day eg. day1, argument provided: " + splittedData[1];

                return data;
            } else if (!Number.isInteger(parseInt(splittedData[2].substr(splittedData[2].length - 1)))) {
                data["error"] = "Error: 2nd argument is missing day number eg. day1, argument provided: " + splittedData[2];
                return data;
            }
        } else {
            data["error"] = "Error: 2nd argument day is missing";
            return data;
        }

        if (splittedData[3]) {
            if (splittedData[3].trim().toLowerCase() == "fs") {} else if (splittedData[3].trim().charAt(0).toLowerCase() != "m") {
                data["error"] = "Error: 3th argument should be match or use fs for final standings eg. match1, argument provided: " + splittedData[3];
                return data;
            } else if (!Number.isInteger(parseInt(splittedData[3].substr(splittedData[3].length - 1)))) {
                data["error"] = "Error: 3th argument is missing match number or use fs for final standings eg. match1, argument provided: " + splittedData[3];
                return data;
            }
        } else {
            data["error"] = "Error: 3th argument match is missing";
            return data;
        }


        if (splittedData[4]) {
            if (splittedData[4].trim() == "") {
                data["error"] = "Error: 4th argument should be team name eg. MRK, argument provided: " + splittedData[4];
                return data;
            }
        } else {
            data["error"] = "Error: 4th argument team name is missing";
            return data;
        }

        data["season"] = splittedData[1].substr(splittedData[1].length - 1);
        data["tier"] = splittedData[2].substr(splittedData[2].length - 1);
        if (splittedData[3].trim().toLowerCase() != "fs")
            data["match"] = splittedData[3].substr(splittedData[3].length - 1);
        else
            data["match"] = splittedData[3].trim().toLowerCase();

        data["teamName"] = splittedData[4].trim();

        return data;
    }
}




function EligibleParser(command) {
    let data = {};
    data["error"] = "";
    var splittedData = command.split("_"); {
        if (splittedData[1]) {
            if (splittedData[1].trim().charAt(0).toLowerCase() != "d") {
                data["error"] = "Error: 1st argument should be day eg. day1, argument provided: " + splittedData[1];
                return data;
            } else if (!Number.isInteger(parseInt(splittedData[1].substr(splittedData[1].length - 1)))) {
                data["error"] = "Error: 1st argument is missing week number eg. day1, argument provided: " + splittedData[1];
                return data;
            }
        } else {
            data["error"] = "Error: 1st argument day is missing";
            return data;
        }

        data["day"] = splittedData[1].substr(splittedData[1].length - 1);
        return data;
    }
}
function ScheduleParser(command) {
    let data = {};
    data["error"] = "";
    var splittedData = command.split("_"); {
        if (splittedData[1]) {
            if (splittedData[1].trim().charAt(0).toLowerCase() != "t" && splittedData[1].trim() != "prime") {
                data["error"] = "Error: 1st argument should be Tier eg. T3 or Prime argument provided: " + splittedData[1];
                return data;
            } else if (!Number.isInteger(parseInt(splittedData[1].substr(splittedData[1].length - 1))) && splittedData[1].trim() != "prime") {
                data["error"] = "Error: 1st argument is missing tier number eg. T2 or prime, argument provided: " + splittedData[1];
                return data;
            }
        } else {
            data["error"] = "Error: 1st argument Tier is missing";
            return data;
        }



        if (splittedData[2]) {
            if (splittedData[2].trim().charAt(0).toLowerCase() != "g" && splittedData[1].trim() != "prime") {
                data["error"] = "Error: 2nd argument should be group eg. groupA, argument provided: " + splittedData[3];
                return data;
            }

        } else {
            if (splittedData[1].trim() != "prime") {
                data["error"] = "Error: 2nd argument group is missing";
                return data;
            }
        }

        if (splittedData[1].trim() != "prime") {
            data["tier"] = splittedData[1].substr(splittedData[1].length - 1);
            data["group"] = splittedData[2].substr(splittedData[2].length - 1);
        } else {
            data["group"] = "z";
            data["tier"] = "prime"

        }
        return data;
    }
}

function ValidationParser(inputString) {
    let data = {};
    data["error"] = "";
    
    let splittedDataTeamData = helper.RemoveNewLines(inputString.split("\n"));
    let teamNameLine = "";
    let teamNameLineSplitted;

    if (splittedDataTeamData.length > 1) {
        teamNameLine = splittedDataTeamData[1];
        teamNameLineSplitted = teamNameLine.split(":");
        if (teamNameLineSplitted.length != 2) {
            data["error"] = "Error: There is a mistake in team name line.";
            return data;
        } else {
            if (teamNameLineSplitted[0].toLowerCase().trim().includes("team name")) {
                if (teamNameLineSplitted[1].trim().includes("<@") && teamNameLineSplitted[1].trim().includes(">")) {
                    data["error"] = "Error: You can not tag players in Team name line.";
                    return data;
                } else if (teamNameLineSplitted[1].trim() != "") {
                    data["TeamName"] = teamNameLineSplitted[1].trim();
                    return data;
                } else {
                    data["error"] = "Error: Team name line format is invalid.";
                    return data;
                }
            } else {
                data["error"] = "Error: Team name line not found.";
                return data;
            }
        }
    }
    else
    {
        data["error"] = "Error: Validation format error.";
        return data;
    }    
}


function MOSParser(inputString) {
    let data = {};
    data["error"] = "";
    
    let splittedDataTeamData = helper.RemoveNewLines(inputString.split("\n"));
    let teamNameLine = "";
    let teamNameLineSplitted;

    if (splittedDataTeamData.length > 1) {
        teamNameLine = splittedDataTeamData[1];
        teamNameLineSplitted = teamNameLine.split(":");
        if (teamNameLineSplitted.length != 2) {
            data["error"] = "Error: There is a mistake in team name line.";
            return data;
        } else {
            if (teamNameLineSplitted[0].toLowerCase().trim().includes("team name")) {
                if (teamNameLineSplitted[1].trim().includes("<@") && teamNameLineSplitted[1].trim().includes(">")) {
                    data["error"] = "Error: You can not tag players in Team name line.";
                    return data;
                } else if (teamNameLineSplitted[1].trim() != "") {
                    data["TeamName"] = teamNameLineSplitted[1].trim();
                    return data;
                } else {
                    data["error"] = "Error: Team name line format is invalid.";
                    return data;
                }
            } else {
                data["error"] = "Error: Team name line not found.";
                return data;
            }
        }
    }
    else
    {
        data["error"] = "Error: Validation format error.";
        return data;
    }    
}



function TeamNameParser(inputString) {
    let data = {};
    data["error"] = "";
    
    let splittedDataTeamData = helper.RemoveNewLines(inputString.split("\n"));
    let teamNameLine = "";
    let teamNameLineSplitted;

    if (splittedDataTeamData.length > 1) {
        teamNameLine = splittedDataTeamData[1];
        teamNameLineSplitted = teamNameLine.split(":");
        if (teamNameLineSplitted.length != 2) {
            data["error"] = "Error: There is a mistake in team name line.";
            return data;
        } else {
            if (teamNameLineSplitted[0].toLowerCase().trim().includes("team name")) {
                if (teamNameLineSplitted[1].trim().includes("<@") && teamNameLineSplitted[1].trim().includes(">")) {
                    
                    data["TeamName"] = teamNameLineSplitted[1].trim().split('<@')[0].trim();
                    return data;
                } else if (teamNameLineSplitted[1].trim() != "") {
                    data["TeamName"] = teamNameLineSplitted[1].trim();
                    return data;
                } else {
                    data["error"] = "Error: Team name line format is invalid.";
                    return data;
                }
            } else {
                data["error"] = "Error: Team name line not found.";
                return data;
            }
        }
    }
    else
    {
        data["error"] = "Error: Validation format error.";
        return data;
    }    
}

module.exports = {
    ResParser,
    MOSParser,
    EligibleParser,
    ResultsParser,
    ScheduleParser,
    ValidationParser,
    TeamNameParser
};
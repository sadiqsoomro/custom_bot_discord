function doInfoLog(reqPathUrl, descriptionForLogData, dataToLog) {
    const log = require('../../log-config').getLogger("logs")
    log.info(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
}

function doErrorLog(reqPathUrl, descriptionForLogData, dataToLog) {
    const log = require('../../log-config').getLogger("logs")
    log.error(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
}

function doFatalLog(reqPathUrl, descriptionForLogData, dataToLog) {
    const log = require('../../log-config').getLogger("logs")
    log.fatal(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
}

module.exports = {
    doInfoLog,
    doErrorLog,
    doFatalLog
}
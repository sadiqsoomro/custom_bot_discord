function RemoveLastCharacter(str) {
    if (str != null && str.length > 0 ) {
        str = str.substring(0, str.length - 1);
    }
    return str;
}


function RemoveNewLines (arrayData){
    let array = arrayData;
    for(var i = array.length - 1; i >= 0; i--) {
        if(array[i].trim() == "") {
            array.splice(i, 1);
        }
    }
    return array;
}

function UserHasRoleByID(Message,RoleID) {
    if(Message.member.roles.cache.find(r => r.id == RoleID))
    return true;
    else
    return false;
}

function UserHasRoleByName(Message,RoleName) {
    if(Message.member.roles.cache.find(r => r.name.toLowerCase() == RoleName.toLowerCase()))
    return true;
    else
    return false;
}

function FindChannelByIDFromMember(Member,ChannelID)
{
    const channel = Member.guild.channels.cache.find(ch => ch.id == ChannelID);
    return channel;
}


function CommandValidator(inputString) {
    if (inputString.split("\n").length > 1)
        return inputString.split("\n")[0]
    else if (inputString.split(" ").length > 1)
        return inputString.split(" ")[0]
    else
        return inputString;
}

function CurrentMosDay()
{
    const currentDay = new Date().toString("en-US", {timeZone: "Asia/Karachi"}).substring(0,3);
    const objectDays = {'Mon':1,'Tue':2,'Wed':3,'Thu':4,'Fri':5,'Sat':1,'Sun':1}

    return objectDays[currentDay]
}

function CurrentT3Day()
{
    const currentDay = new Date().toString("en-US", {timeZone: "Asia/Karachi"}).substring(0,3);
    const objectDays = {'Mon':1,'Tue':1,'Wed':1,'Thu':1,'Fri':1,'Sat':1,'Sun':2}

    return objectDays[currentDay]
}
function CurrentT2Day()
{
    const currentDay = new Date().toString("en-US", {timeZone: "Asia/Karachi"}).substring(0,3);
    const objectDays = {'Mon':1,'Tue':2,'Wed':3,'Thu':4,'Fri':5,'Sat':1,'Sun':1}

    return objectDays[currentDay]
}

function CurrentT1Day()
{
    const currentDay = new Date().toString("en-US", {timeZone: "Asia/Karachi"}).substring(0,3);
    const objectDays = {'Mon':1,'Tue':2,'Wed':3,'Thu':4,'Fri':5,'Sat':1,'Sun':1}

    return objectDays[currentDay]
}

function CurrentPrimeDay()
{
    const currentDay = new Date().toString("en-US", {timeZone: "Asia/Karachi"}).substring(0,3);
    const objectDays = {'Mon':1,'Tue':2,'Wed':3,'Thu':4,'Fri':5,'Sat':1,'Sun':1}

    return objectDays[currentDay]
}


function Shuffle(str) {
  
    var a = str;
    var newArr = [];
    var neww = '';
    var text = a.replace(/[\r\n]/g, '').split(' ');
    
    text.map(function(v) {
      v.split('').map(function() {
        var hash = Math.floor(Math.random() * v.length);
        neww += v[hash];
        v = v.replace(v.charAt(hash), '');
      });
      newArr.push(neww);
      neww = '';
    });
    var x = newArr.map(v => v.split('').join('')).join('\n');
    return x.split('').map(v => v.toUpperCase()).join('');
  }
module.exports = {
    Shuffle,
    FindChannelByIDFromMember,
    RemoveLastCharacter,
    RemoveNewLines,
    CommandValidator,
    CurrentMosDay,
    CurrentT3Day,
    CurrentT2Day,
    CurrentT1Day,
    CurrentPrimeDay
}